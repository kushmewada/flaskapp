from flask import Flask, redirect, render_template, Blueprint, request, session as ses
from flask_mail import Mail
from flask.cli import AppGroup
from flask_sqlalchemy import SQLAlchemy
import json
# import model
import os
import click
from werkzeug.utils import secure_filename
from datetime import datetime


with open('config.json', 'r') as f:
    params = json.load(f)['params']

app = Flask(__name__)

app.secret_key = "secretKey1"
app.config['FLASK_ENV'] = 'development'
app.config['UPLOAD_FOLDER'] = params['upload_location']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=True

app.config.update(MAIL_SERVER='smtp.gmail.com', MAIL_PORT = 465, MAIL_USERNAME=params['user_gamil'], MAIL_PASSWORD=params['gmail_password'], MAIL_USE_SSL=True)
mail = Mail(app)
local_server = bool(params['local_server'])

if local_server:
    app.config['SQLALCHEMY_DATABASE_URI'] = params['local_uri']

else:
    app.config['SQLALCHEMY_DATABASE_URI'] = params['prod_uri']

db = SQLAlchemy(app)




class User(db.Model):
    sNo = db.Column(db.Integer, primary_key=True, autoincrement=True)
    fName = db.Column(db.String(50))
    lName = db.Column(db.String(50))
    image = db.Column(db.String(20), nullable=True)
    email = db.Column(db.String(100))
    password = db.Column(db.String(100))
    createdAt = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)


    def __repr__(self):
        return f'User {self.fName} {self.lName}'

class Myses(db.Model):
    sNo = db.Column(db.Integer, primary_key=True)
    skey = db.Column(db.String(50))
    svalue = db.Column(db.String(100), nullable=True)
    createdAt = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)

    def __repr__(self):
        return f'Key: {self.svalue}'

class Contact(db.Model):
    sNo = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50))
    title = db.Column(db.String(50))
    msg = db.Column(db.String(300))
    createdAt = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)

    def __repr__(self):
        return f'User {self.email}'



# @app.route('/')
group_click = AppGroup('main')

@group_click.command('create')
@click.argument('name')
def greeting(name):
    print(f"Hello {name}")

bluePri = Blueprint('students', __name__)
@bluePri.cli.command("create")
@click.argument('name')
def Students(name):
    print(f'student {name}')

app.cli.add_command(group_click)
app.register_blueprint(bluePri)

@app.route('/')
def Home():
    user = Myses.query.filter_by(skey="user").first()
    # print(user.svalue)
    if user:
        alert = 'You are logged in'
        return render_template('index.html', alert=alert, params = params, user=user)
    return render_template('index.html', params = params)

@app.route('/login')
def LoginView():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        return redirect('/')
    return render_template('login.html', params = params)


@app.route('/login/post',methods = ['POST', 'GET'])
def Login():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        return redirect('/')
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['pass']
        field = User.query.filter_by(email=email).first()
        alert = "please enter registered email"
        if field:
            alert = "email and password not match!"
            if password == field.password:
                myses = Myses(skey="user", svalue=email)
                db.session.add(myses)
                db.session.commit()
                alert = "You are logged in!"
                return redirect('/')

        return render_template('login.html', alert = alert, params = params)
        
    return redirect('/login')



@app.route('/logout')
def logout():
    allses = Myses.query.all()
    for ses in allses:
        db.session.delete(ses)
    db.session.commit()

    return redirect('/')

@app.route('/signup')
def signupView():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        return redirect('/')
    return render_template('signup.html', params = params)


@app.route('/signup/post', methods = ['POST', 'GET'])
def signup():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        return redirect('/')
    if request.method == 'POST':
        fName = request.form['fName']
        lName = request.form['lName']
        email = request.form['email']
        password = request.form['pass']
        password1 = request.form['pass2']

        if password != password1:
            alert = "Password not match! please enter same password"
            return render_template('signup.html', alert = alert, params = params)
        createUser = User(fName=fName, lName = lName, email=email, password = password)
        db.session.add(createUser)
        db.session.commit()
        return redirect('/')
    return redirect('/')

@app.route('/contact')
def contactView():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        return render_template('contact.html', params = params, user=user)
    return render_template('contact.html', params = params)


@app.route('/contact/post', methods=['POST', 'GET'])
def contact():
    if request.method == 'POST':
        email = request.form['email']
        title = request.form['title']
        msg = request.form['msg']
        data = Contact(email=email, title=title, msg=msg)
        db.session.add(data)
        db.session.commit()
        mail.send_message(
            f"{params['web_name']} {email}",
            sender = email,
            recipients = [params['user_gamil']],
            body = msg
        )
    return redirect('/')

@app.route('/profile')
def profile():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        userP = User.query.filter_by(email=user.svalue).first()
        return render_template('profile.html', params=params, user=user, data=userP, file=user.svalue)
    return redirect('/')

@app.route('/profile/edit')
def profileEdit():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        userP = User.query.filter_by(email=user.svalue).first()
        update = True
        return render_template('profile.html', params=params, user=user, data=userP, update = update, file=user.svalue)

    return redirect('/')

@app.route('/profile/updating', methods=['POST', 'GET'])
def profileUpdate():
    user = Myses.query.filter_by(skey="user").first()
    if user:
        if request.method == 'POST':
            update = User.query.filter_by(email=user.svalue).first()
            password = request.form['pass']
            if password != update.password:
                alert = 'Please enter correct password!'
                up = True
                return render_template('profile.html', alert= alert, params=params, user=user, data=update, update = up, file=user.svalue)
            first = request.form['fName']
            last = request.form['lName']

            # print(first)
            # print(last)
            update.fName = first
            update.lName = last
            db.session.commit()
            return redirect('/profile')

        return redirect('/')

    return redirect('/') 


@app.route('/profile/upload', methods=['POST'])
def profileUpload():
    user = Myses.query.filter_by(skey="user").first()
    if user:   
        if request.method == 'POST':
            file = request.files['file']
            field = User.query.filter_by(email=user.svalue).first()
            field.image = file.filename
            db.session.add(field)
            db.session.commit()
            if file:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file.filename)))
                sesfile = Myses(skey="file", svalue=secure_filename(file.filename))
                db.session.add(sesfile)
                db.session.commit()
                return redirect('/profile')
            return redirect('/profile')
        return redirect('/')
        
    return redirect('/')




if __name__ == "__main__":
    app.run(debug=True, host="localhost", port=2000)
