from importlib.metadata import entry_points
from setuptools import setup

setup(
    name = 'flask-my-ext',
    entry_points = {
        'flask.commands':[
            'my-command=flask_my_ext.command:cli'
        ],
    },
)