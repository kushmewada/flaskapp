import myApp
from datetime import datetime

db = myApp.db

class User(db.Model):
    sNo = db.Column(db.Integer, primary_key=True, autoincrement=True)
    fName = db.Column(db.String(50))
    lName = db.Column(db.String(50))
    image = db.Column(db.String(20), nullable=True)
    email = db.Column(db.String(100))
    password = db.Column(db.String(100))
    createdAt = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)


    def __repr__(self):
        return f'User {self.fName} {self.lName}'

class Myses(db.Model):
    sNo = db.Column(db.Integer, primary_key=True)
    skey = db.Column(db.String(50))
    svalue = db.Column(db.String(100), nullable=True)
    createdAt = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)

    def __repr__(self):
        return f'Key: {self.svalue}'

class Contact(db.Model):
    sNo = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50))
    title = db.Column(db.String(50))
    msg = db.Column(db.String(300))
    createdAt = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)

    def __repr__(self):
        return f'User {self.email}'